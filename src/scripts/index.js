import { EventEmitter } from 'events'
import html from 'yo-yo'
import mainContent from './components/main-content'
import responseDataJson from './response.data.json' 

var $root = document.querySelector('#maincontent')
var bus = new EventEmitter()
var initialState = {
  previousQuestions: [
    'allergy season asthma',
    'best treatment for asthma',
    'chronic asthma comorbidity',
    'homeopathic treatment',
    'comorbitidies',
    'steroid use in treating asthma',
  ],
  suggestions: [],
  currentQuestion: '',
  responseData: {},
  showButtons: false,
  showSuggestions: false
}

store(bus, initialState)
window.addEventListener('load', function(){
  bus.emit('state-change')
})

function store (bus, state = {}) {
  bus.on('input-change', function (value) {
    state.suggestions = state.previousQuestions.filter(q => q.match(value)) 
    state.showSuggestions = true
    bus.emit('state-change')
  })

  bus.on('ask', function (question) {
    state.currentQuestion = question
    state = Object.assign({}, initialState, { responseData: responseDataJson, showButtons: true, showSuggestions: false })
    bus.emit('state-change')
  })

  bus.on('state-change', function(){
    render(bus, state)
  })

  function render (bus, props) {
    console.log('update...')
    html.update($root, mainContent(bus, props)) 
  }
}
