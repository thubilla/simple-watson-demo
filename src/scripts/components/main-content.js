import html from 'yo-yo'
import sampleQA from './sample'
import answer from './answer'
import industryAssets from './industry-assets'

function mainContent (bus, props) {
  return html`
    <main id="maincontent" class="container">
    <form id="watson-search" class="input-group input-group-lg" onsubmit=${submitHandler}>
      <input id="watson-search-input" type="text" class="form-control" placeholder="Ask your question here!" onkeyup=${changeHandler}>
      <div class="input-group-btn">
        <button id="watson-send-query-btn" class="btn" type="submit">
          <span class="glyphicon glyphicon-search"></span>
        </button>
      </div>
    </form>
    <menu id="suggestions" class="list-group ${props.showSuggestions ? 'show' : ''}">
      ${props.suggestions.length ? html`<h5>Previous Questions</h5>` : ''}
      ${props.suggestions.map(s => html`<li class="list-group-item">
        <a onclick=${historyHandler}>${s}</a>
      </li>`)}
    </menu>
    <div id="watson-response">
      <div class="watson-btns ${props.showButtons ? 'show' : ''}">
        <button id="prev-answer" class="btn btn-default watson-btn pull-left" data-direction="previous" onclick=${clickHandler}>
          <span class="glyphicon glyphicon-chevron-left"></span>
        </button>
        <button id="next-answer" class="btn btn-default watson-btn pull-right" data-direction="next" onclick=${clickHandler}>
          <span class="glyphicon glyphicon-chevron-right"></span>
        </button>
      </div>
      <div id="watson-response-area">
        ${
          !props.responseData.details 
            ? sampleQA()
            : html`<ul>${props.responseData.details.response.docs.map((doc, i) => answer(bus, {...doc, index: i}))}</ul>` 
         }

        ${
          !props.responseData.details
            ? ''
            : industryAssets(bus, props.responseData.details.industryassets)
         }
      </div>
    </div>
  </main>
  `

  function changeHandler (e) {
    var { value } = e.target 
    if(!value.trim()){
      return
    }
    bus.emit('input-change', value)
  }

  function submitHandler (e) {
    e.preventDefault()

    var question = e.target.querySelector('input').value
    bus.emit('ask', question)

    e.target.reset()
  }

  function historyHandler () {
    bus.emit('ask', 'dummy question')
  }

  function clickHandler (e) {
    var sharedParent = e.target.closest('#watson-response')
    var current = sharedParent.querySelector('.active')
    var step = current[`${e.target.dataset.direction}ElementSibling`]

    if(step){
      current.classList.remove('active')
      step.classList.add('active')
    } else {
      alert(`🛑 No ${e.target.dataset.direction} answer 😣`) 
    }
  }  
}

export default mainContent
