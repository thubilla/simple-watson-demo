import html from 'yo-yo'

function industryAssets (bus, props) {
  return html`
    <div class="industry-assets">
      <h3>Industry Sponsored Assets</h3>
      <ul class="list-group">
        ${props.map(asset => industryAsset(bus, asset))} 
      </ul>
    </div>
  `
}

function industryAsset(bus, prop) {
  return html`
    <li class="list-group-item">
      <a href="${prop.url}" target="_blank">
        <h5>${prop.title}</h5>
      </a>
    </li> 
  `
}

export default industryAssets
