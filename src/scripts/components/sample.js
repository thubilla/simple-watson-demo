import html from 'yo-yo'

function sampleQA () {
  var sample = {
    question: 'Are there differences in the mortality rate of asthma between adults and children?',
    answer: 'Asthma is a chronic airway disorder that affects more than 25 million people in the United States. Asthma typically begins during childhood and can occur during all stages of life, but clinical manifestations can change with age, and many children with asthma are free of the disease later in life....'
  }
  return html`
    <div id="sample-qa-container">
      <span class="sample">Sample Question</span>
      <span id="sample-question" class="sample">${sample.question}</span>
      <span class="sample">Sample Answer</span>
      <p id="sample-answer">${sample.answer}</p>
    </div> 
  `
}

export default sampleQA
