import html from 'yo-yo'

function answer (bus, props) {
  return html`
  <li class="watson-response-text ${props.index > 0 ? '' : 'active'}" data-id="${props.id}">
    <div id="${props.id}">
      <p class="answer-text" data-answerid="${props.id}">${props.html_text[0]}</p>
    </div>
    <div class="learn-more-area">
      <div class="form-group">
        <select name="ratings" class="thumbsdn-ratings display-none form-control" data-answerid="${props.id}">
          <option value="">Please Select a Reason for your Rating</option>
          <option value="2">Inaccurate</option>
          <option value="3">Not Specific Enough</option>
          <option value="4">Not Relevant</option>
          <option value="5">Out of Date</option>
        </select>
      </div>
      <div class="btn btn-default watson-icon-btn learn-more-btn" data-answerid="${props.id}">
        <p>
          <a class="learn-more-link" target="_blank" href="https://api.atpoc.com/suite/#library/${props.ta[0]}/${props.learnmore_web.split('index.html#/')[1]}" data-answerid="${props.id}">Learn <br>More</a>
        </p>
      </div>
      <div class="btn btn-default watson-icon-btn wtsn-thumbsup-btn" data-answerid="${props.id}">
        <span class="glyphicon glyphicon-thumbs-up"></span>
      </div>
      <div class="btn btn-default watson-icon-btn wtsn-thumbsdn-btn" data-answerid="${props.id}">
        <span class="glyphicon glyphicon-thumbs-down"></span>
      </div>
    </div>
  </li>
  `
}

export default answer
